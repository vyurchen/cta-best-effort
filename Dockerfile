FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

RUN yum -y install epel-release
RUN yum install -y make gcc rpm-build git cronie python36-flask python36-requests

RUN mkdir -p /usr/lib/python3.6/site-packages/utils
COPY src/utils/utils.py /usr/lib/python3.6/site-packages/utils/utils.py
COPY src/cta-best-effort.py /usr/bin/cta-best-effort.py
COPY src/process_manager.py /usr/bin/process_manager.py
COPY src/mattermost_feedback_server.py /usr/bin/mattermost_feedback_server.py
COPY src/ticket_manager.py /usr/bin/ticket_manager.py

RUN mkdir -p /etc/cron.d/
COPY src/cta-best-effort.cron /etc/cron.d/cta-best-effort.cron

RUN mkdir -p /etc/cta-best-effort/
COPY src/availability.json /etc/cta-best-effort/availability.json
COPY src/blacklisted_instances.yaml /etc/cta-best-effort/blacklisted_instances.yaml
COPY src/mattermost_webhook_url /etc/cta-best-effort/mattermost_webhook_url
COPY src/cta_best_effort_channel_id /etc/cta-best-effort/cta_best_effort_channel_id


#install supervisor
RUN yum install -y supervisor

COPY "supervisord" "/etc/supervisor/conf.d/supervisord.conf"

# Open port for mattermost_feedback_server.py that handles the MM channel slash commands
EXPOSE 5000

# Start supervisor
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
