#!/usr/bin/env python3

from utils.utils import send_mattermost_message
import flask
import requests
from flask import Response
from flask import request
import json
import os
# import ticket timer killer module
import process_manager

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/ctaalertack', methods=['POST'])
def workingonit():
    print(request)    

    file = open("/etc/cta-best-effort/mattermost_webhook_url","r")
    mattermost_hook = file.read()
    mattermost_hook = mattermost_hook.rstrip()
    # mm channel id
    file = open("/etc/cta-best-effort/cta_best_effort_channel_id","r")
    cta_best_effort_channel_id = file.read()
    cta_best_effort_channel_id = cta_best_effort_channel_id.rstrip()

    # get who is working on it and from where
    username = request.form['user_name']
    mm_channel_name = request.form['channel_name']
    mm_channel_id = request.form['channel_id']
    
    if cta_best_effort_channel_id != mm_channel_id:
        
        print("Unathorized: " + username + " from " + mm_channel_name)
    
    elif os.system("ps ax | grep ticket_manager.py | grep -q -v grep") == 0:

        message_payload = {
                    "text": "CTA alert acknowledged, **" + username + "** is working on it, stopping snow timer..."
        }

        send_mattermost_message(mattermost_hook, message_payload)

        process_manager.killProcess()

    else:
        message_payload = {
                    "text": "CTA alert acknowledged, **" + username + "** is working on it"
        }

        send_mattermost_message(mattermost_hook, message_payload)
 
    return Response(status=200, mimetype='application\json')

app.run(host= '0.0.0.0')
