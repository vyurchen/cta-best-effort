#!/usr/bin/env python3

from utils.utils import send_mattermost_message
import process_manager
import json
import os
import subprocess
import time
import sys

file = open("/etc/cta-best-effort/mattermost_webhook_url","r")
mattermost_hook = file.read()
mattermost_hook = mattermost_hook.rstrip()

f = open('/etc/cta-best-effort/availability.json')
data = json.load(f)

if not os.path.exists('/etc/cta-best-effort/best-effort-lock'):

    for instance in data:
    
        instance_name = instance
        availability = data[instance]['availability']
        # we don't want alarms for all instances
        send_alerts = data[instance]['send_alerts']
        production = data[instance]['production']
    
    
        # if instance is available again, the timer kills itself
        if os.system("ps ax | grep \"ticket_manager.py \--instance " + instance_name + "\" | grep -q -v grep") == 0:

            for line in os.popen("ps ax | grep \"ticket_manager.py \--instance " + instance_name + "\" | grep -v grep"):
                fields = line.split()
                # exact name check
                instance_in_process = fields[7]
                if availability == "available" and instance_name == instance_in_process:
                    process_manager.killInstanceProcess(instance_name)
            
        if availability == "unavailable" and send_alerts == "yes" and production == "yes":

            subprocess.Popen([sys.executable, '/usr/bin/ticket_manager.py', '--instance', instance_name], shell=False)
            time.sleep(1)    
