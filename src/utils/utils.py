#!/usr/bin/env python3

import requests


def send_mattermost_message(webhook_url, payload, channel=None):
    content_header = {'Content-Type': 'application/json'}
    if channel:
        payload['channel'] = channel
    requests.post(webhook_url, headers=content_header, json=payload)
