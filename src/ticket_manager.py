#!/usr/bin/env python3

from utils.utils import send_mattermost_message
import os
import time
import optparse
import json
import requests

def getAccessToken():
    data = "grant_type=client_credentials&client_id=cta-snow&client_secret=16be0e3d-d07b-49a1-a173-7e0c9d767bb3&audience=snow-api-sso-protected"
    headers = {'Content-Type': 'application/x-www-form-urlencoded' }

    response = requests.post('https://auth.cern.ch/auth/realms/cern/api-access/token', data=data, headers=headers)
    result = response.json()

    return result['access_token']

def startTicketTimer():

    file = open("/etc/cta-best-effort/mattermost_webhook_url","r")
    mattermost_hook = file.read()
    mattermost_hook = mattermost_hook.rstrip()

    # load unavailability reasons
    parser = optparse.OptionParser()
    
    parser.add_option('--instance', type='string', dest='unavailable_instance')
    (options, args) = parser.parse_args()
    instance_name = options.unavailable_instance

    # we check if a ticket for the instance is already in the process of being created
    PROCESS_NAME = "\"ticket_manager.py --instance " + instance_name + "\""
    process_checker = list(os.popen("ps ax | grep " + PROCESS_NAME + " | grep -v grep"))    

    if not process_checker or len(process_checker) == 1:

        message_payload = {
            "text": "@channel **" + instance_name + "** is unavailable, please issue **/ctaalertack** to stop the snow ticket creation (1 hour timer)"
        }

        send_mattermost_message(mattermost_hook, message_payload)

        # timer starts
        TIME_TO_WAIT = 3600
        time.sleep(TIME_TO_WAIT)

        f = open('/etc/cta-best-effort/availability.json')
        data = json.load(f)

        message = """Dear CTA operator,
This is an automated test message. Please ignore.
EOSCTA instance {0} is unavailable. 
"""
 
        message = message.format(instance_name)
        incident_attributes_to_insert = {
            'short_description' : '[IGNORE] EOSCTA Test Best Effort - ' + instance_name + ' is unavailable',
            'u_business_service' : '50a09678dbfcbb04026175db8c961929', # CTA Service
            'u_functional_element' : '184b875ddba27f808f2f177e169619c9', # EOSCTA
            'comments': message,
            }
        data = json.dumps(incident_attributes_to_insert)

        access_token = getAccessToken()
        headers = {'Authorization': 'Bearer ' + access_token}
        
        try:
            response = requests.post('https://cerntraining.service-now.com/api/now/v2/table/incident', data=data, headers=headers)
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)

        result = response.json()
        req = result['result']
        ticket_number = req['number']

        message_payload = {
               "text": "Automatic snow ticket created related to " + instance_name + ": " + ticket_number
               }

        send_mattermost_message(mattermost_hook, message_payload)

startTicketTimer()
