#!/usr/bin/env python3

import os, signal, time
from utils.utils import send_mattermost_message
  
def killProcess():
     
    PROCESS_NAME  = "ticket_manager.py"
    file = open("/etc/cta-best-effort/mattermost_webhook_url","r")
    mattermost_hook = file.read()
    mattermost_hook = mattermost_hook.rstrip()

    try:
         
        for line in os.popen("ps ax | grep " + PROCESS_NAME + " | grep -v grep"):
            fields = line.split()
             
            # extracting Process ID from the output
            pid = fields[0]
             
            # terminating process
            os.kill(int(pid), signal.SIGKILL)

        message_payload = {
            "text": "Automatic snow ticket creation stopped, going on a break for 30 minutes..."
            }

        send_mattermost_message(mattermost_hook, message_payload)


        # create lock to avoid multiple timers when someone is working on it
        if not os.path.exists('/etc/cta-best-effort/best-effort-lock'):
            with open('/etc/cta-best-effort/best-effort-lock', 'w'): pass

        # kill lock and reactivate best effort procedure after 30 minutes
        TIME_TO_WAIT = 1800
        time.sleep(TIME_TO_WAIT)

        if os.path.exists("/etc/cta-best-effort/best-effort-lock"):
          os.remove("/etc/cta-best-effort/best-effort-lock")
         
    except:

       message_payload = {
           "text": "Error occured while trying to stop the snow timer"
           }

       send_mattermost_message(mattermost_hook, message_payload)


def killInstanceProcess(instance_name):

    #PROCESS_NAME  = "ticket_manager.py"
    file = open("/etc/cta-best-effort/mattermost_webhook_url","r")
    mattermost_hook = file.read()
    mattermost_hook = mattermost_hook.rstrip()

    try:

        if os.popen("ps ax | grep \"ticket_manager.py \--instance " + instance_name + "\" | grep -v grep"):
            for line in os.popen("ps ax | grep \"ticket_manager.py \--instance " + instance_name + "\" | grep -v grep"):
                fields = line.split()

                # extracting Process ID from the output
                pid = fields[0]

                # terminating process
                os.kill(int(pid), signal.SIGKILL)

                message_payload = {
                    "text": "**" + instance_name + "** is available again, stopping the snow timer"
                    }

                send_mattermost_message(mattermost_hook, message_payload)

    except:

       message_payload = {
           "text": "Error occured while trying to stop the snow timer"
           }

       send_mattermost_message(mattermost_hook, message_payload)


